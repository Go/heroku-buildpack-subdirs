# heroku-buildpack-multi

Use multiple buildpacks on your app

## Usage

    $ heroku config:add BUILDPACK_URL=https://github.com/ddollar/heroku-buildpack-subdirs.git

    $ cat .heroku-subdirs
    subdir/to/node https://github.com/heroku/heroku-buildpack-nodejs.git 0198c71daa8
    subdir/to/go https://github.com/heroku/heroku-buildpack-go.git v86

## License

MIT
